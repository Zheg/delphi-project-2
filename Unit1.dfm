object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 502
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 136
    Top = 120
    object File1: TMenuItem
      Caption = 'File'
      object Exi1: TMenuItem
        Caption = 'Exit'
        OnClick = Exi1Click
      end
    end
    object Windows1: TMenuItem
      Caption = 'Windows'
      object Dialog1: TMenuItem
        Caption = 'Dialog'
        OnClick = Dialog1Click
      end
      object Compute1: TMenuItem
        Caption = 'Compute'
        OnClick = Compute1Click
      end
    end
    object Control1: TMenuItem
      Caption = 'Control'
      object Cascade1: TMenuItem
        Caption = 'Cascade'
        OnClick = Cascade1Click
      end
      object ile1: TMenuItem
        Caption = 'Tile'
        OnClick = ile1Click
      end
      object Closecurrent1: TMenuItem
        Caption = 'Close current'
        OnClick = Closecurrent1Click
      end
      object Closeall1: TMenuItem
        Caption = 'Close all'
        OnClick = Closeall1Click
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
    end
  end
end
