object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 184
    Top = 53
    Width = 38
    Height = 13
    Caption = 'Output:'
  end
  object Edit1: TEdit
    Left =  150
    Top = 48
    Width = 75
    Height = 25
    Text = 'Input number of column'
  end
  object OutCap: TLabel
    Left = 264
    Top = 53
    Width = 37
    Height = 13
    Caption = 'OutCap'
  end
  object Matrix: TStringGrid
    Left = 24
    Top = 96
    Width = 585
    Height = 185
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
    OnClick = MatrixClick
  end
  object Compute1: TButton
    Left = 24
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Compute'
    TabOrder = 1
    OnClick = Compute1Click
  end
  object Exit1: TButton
    Left = 520
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Exit'
    TabOrder = 2
    OnClick = Exit1Click
  end
end
