﻿unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids;

type
  TForm3 = class(TForm)
    Matrix: TStringGrid;
    Compute1: TButton;
    Label1: TLabel;
    OutCap: TLabel;
    Exit1: TButton;
    Edit1: TEdit;
    procedure Exit1Click(Sender: TObject);
    procedure Compute1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MatrixClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation
uses
  Unit1;

{$R *.dfm}

procedure TForm3.Compute1Click(Sender: TObject);
var
  i, j, n, st, temp, k: integer;
  sum, max: real;

begin
  if (rg = 0) then 
  begin
    i := Matrix.FixedRows;
    j := Matrix.FixedCols;
    for i := Matrix.FixedRows to Matrix.RowCount do
    begin
      sum := 0;
      for j := Matrix.FixedCols to Matrix.ColCount do
      begin
        if (Matrix.Cells[i, j] <> ' ') then
        begin
          if (Matrix.Cells[i, j] > 0) then
          begin
            sum := sum + StrToFloat(Matrix.Cells[i, j]);
          end;
        end;
      end;
      if (max < sum) then
      begin
        max := sum;
        n := i;
      end;
    end;
    OutCap.Caption := IntToStr(n);
  end
  else if (rg = 1) then
  begin
    if (Edit1.Text <> '') then
    begin
      k := StrToInt(Edit1.Text);
      if (k < 1) then
      begin
        k := 1;
      end;
    end;
    for j := Matrix.FixedCols to Matrix.ColCount do
    begin
      for i := Matrix.FixedRows to Matrix.RowCount do
      begin
        if ((Matrix.Cells[k, i] <> ' ') and (Matrix.Cells[k, i + 1] <> ' ')) then
        begin
          if (Matrix.Cells[k][i] > Matrix.Cells[k][i + 1]) then
          begin
            temp := StrToFloat(Matrix.Cells[k][i]);
            Matrix.Cells[k][i] := Matrix.Cells[k][i + 1];
            Matrix.Cells[k][i + 1] := FloatToStr(temp);
          end;
        end;
      end;
    end;
  end;
end;

procedure TForm3.Exit1Click(Sender: TObject);
begin
  Close();
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TForm3.MatrixClick(Sender: TObject);
var
 i : integer;
begin
  for i := Matrix.FixedRows to Matrix.RowCount - 1 do
		Matrix.Cells[Matrix.FixedCols - 1, i] := IntToStr(i - Matrix.FixedRows + 1);
	for i := Matrix.FixedCols to Matrix.ColCount - 1 do
		Matrix.Cells[i, Matrix.FixedRows - 1] := intToStr(i - Matrix.FixedCols + 1);

end;

end.
